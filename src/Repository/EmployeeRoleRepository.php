<?php

namespace App\Repository;

use App\Entity\Employee;
use App\Entity\EmployeeRole;
use App\Entity\Role;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EmployeeRole|null find($id, $lockMode = null, $lockVersion = null)
 * @method EmployeeRole|null findOneBy(array $criteria, array $orderBy = null)
 * @method EmployeeRole[]    findAll()
 * @method EmployeeRole[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmployeeRoleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EmployeeRole::class);
    }

    public function insertEmployeeRoles(int $employeeId, array $roles)
    {

        foreach($roles as $roleId){
            $eRole = new EmployeeRole();
            $eRole->setEmployeeId($employeeId)->setRoleId($roleId);
            $this->getEntityManager()->persist($eRole);  
        }
        $this->getEntityManager()->flush();
    }
    // /**
    //  * @return EmployeeRole[] Returns an array of EmployeeRole objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EmployeeRole
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

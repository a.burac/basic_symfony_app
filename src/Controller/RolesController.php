<?php

namespace App\Controller;

use App\Entity\Role;
use App\Repository\RoleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RolesController extends AbstractController
{

    /**
     * @Route("/roles", name="get_roles", methods={"GET"} )
     */
    public function index(Request $request): Response
    {


        $page = intval($request->query->get('page') ?? 1);
        $page = ($page <= 0) ? 1 : $page; 

        $entityManager = $this->getDoctrine()->getManager();
        $repo = $entityManager->getRepository(Role::class);

        $offset =  $_ENV['PAGE_LIMIT'] * ($page-1);
        $user = $this->getUser();
        $isAdmin = $this->isGranted('ROLE_ADMIN');
        $filters = $isAdmin ? [] : ["companyId" => $user->getCompanyId()];
        $data = $repo->findBy($filters, null, $_ENV['PAGE_LIMIT'], $offset);     

        $result = [];
        foreach($data as $id => $role){
            $result[$id] = [
                "id" => $role->getId(),
                "name" => $role->getName()
            ];
            if($isAdmin){
                $result[$id]['companyId'] = $role->getCompanyId();
            }
        }

        return $this->json([
            'message' => $result
        ]);
    }
    /**
     * @Route("/roles/{id}", name="get_role", methods={"GET"} )
     */
    public function show(int $id): Response
    {
        
        $entityManager = $this->getDoctrine()->getManager();
        $repo = $entityManager->getRepository(Role::class);

        $user = $this->getUser();
        $isAdmin = $this->isGranted('ROLE_ADMIN');
        $filters = ["id" => $id];
        
        if(!$isAdmin){
            $filters["companyId"] = $user->getCompanyId();
        }
        $role = $repo->findOneBy($filters);  
        if(!$role){
            return $this->json(['message' => "not found"], 404);
        }
        $result = [
            "name" => $role->getName(),

        ];
        if($isAdmin){
            $result["companyId"] = $role->getCompanyId();
        }
        return $this->json(["message" => $result]);
    }

    /**
     * @Route("/roles", name="add_role", methods={"POST"})
     */
    public function add(Request $request, ValidatorInterface $validator): Response
    {
        $data = json_decode($request->getContent(), true);

        $constraint = new Assert\Collection([
            
            'name' => [new Assert\Type(['type' => 'string']), new Assert\Length(['min' => 3, 'max' => 255])],
            'companyId' => new Assert\Optional(new Assert\Type(['type' => 'int'])),
        ]);
 
        $errors = $validator->validate($data, $constraint);
        if(!empty($errors[0])){
            return $this->json([
                'message' => $errors[0]->getMessage()
            ]);
        }
        


        $entityManager = $this->getDoctrine()->getManager();
        /**
         * @var RoleRepository $roleRepo
         */
        $roleRepo = $entityManager->getRepository(Role::class);
        $user = $this->getUser();
    
        if($this->isGranted('ROLE_ADMIN')){
            if(empty($data["companyId"])){
                return $this->json([
                    'message' => 'The company ID is required.'
                ]);
            }
            $companyId = $data["companyId"];
         
        }else{
            $companyId = $user->getCompanyId();
        }

        $role = new Role();
        $role->setName($data['name']);
        $entityManager->persist($role);

        try{
            $entityManager->flush();
        }catch(\Exception $e){
            // hot fix because of validator fail
            return $this->json([
                'message' => 'Something went wrong.'
            ]);
        }
        return $this->json([
            'message' => 'Successful'
        ]);
    }

    /**
     * @Route("/roles/{id}", name="update_role", methods={"PUT"})
     */
    public function update(Request $request, ValidatorInterface $validator, int $id): Response
    {
        $data = json_decode($request->getContent(), true);

        $constraint = new Assert\Collection([
            'name' => new Assert\Optional(
                [new Assert\Type(['type' => 'string']), new Assert\Length(['min' => 3, 'max' => 255])]
                ),
        ]);
 
        $errors = $validator->validate($data, $constraint);
        if(!empty($errors[0])){
            return $this->json([
                'message' => $errors[0]->getMessage()
            ]);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $repo = $entityManager->getRepository(Role::class);

        $filters = ['id' => $id];
        if(!$this->isGranted('ROLE_ADMIN')){
            $filters['companyId'] = $this->getUser()->getCompanyId();;
        }
        $role = $repo->findOneBy($filters);
        if(!$role){
            return $this->json(['message' => "not found"], 404);
        }

    
        $role->setName($data['name'] ?? $role->getFirstName());
        $entityManager->persist($role);
        $entityManager->flush();


        return $this->json([
            'message' => 'Successful'
        ]);
    }

    /**
     * @Route("/roles/{id}", name="delete_role", methods={"DELETE"})
     */
    public function delete(int $id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repo = $entityManager->getRepository(Role::class);

        $filters = ['id' => $id];
        if(!$this->isGranted('ROLE_ADMIN')){
            $filters['companyId'] = $this->getUser()->getCompanyId();;
        }
        $role = $repo->findOneBy($filters);
        if(!$role){
            return $this->json(['message' => "not found"], 404);
        }
        if($role){
            $entityManager->remove($role);
            $entityManager->flush();
        }
        
        return $this->json([
            'message' => 'Successful'
        ]);
    }
}

<?php

namespace App\Controller;

use App\Entity\Role;
use App\Entity\Employee;
use App\Entity\EmployeeRole;
use App\Message\EmailNotification;
use App\Repository\RoleRepository;
use Symfony\Bundle\MakerBundle\Validator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Optional;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\GroupSequence;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mime\Email;

class EmployeesController extends AbstractController
{

    /**
     * @Route("/employees", name="get_employees", methods={"GET"} )
     */
    public function index(Request $request): Response
    {


        $page = intval($request->query->get('page') ?? 1);
        $page = ($page <= 0) ? 1 : $page; 

        $entityManager = $this->getDoctrine()->getManager();
        $repo = $entityManager->getRepository(Employee::class);

        $offset =  $_ENV['PAGE_LIMIT'] * ($page-1);
        $user = $this->getUser();
        $isAdmin = $this->isGranted('ROLE_ADMIN');
        $filters = $isAdmin ? [] : ["companyId" => $user->getCompanyId()];
        $data = $repo->findBy($filters, null, $_ENV['PAGE_LIMIT'], $offset);     

        $result = [];
        foreach($data as $id => $employee){
            $result[$id] = [
                "id" => $employee->getId(),
                "firstName" => $employee->getFirstName(),
                "lastName" => $employee->getLastName(),
                "email" => $employee->getEmail()
            ];
            if($isAdmin){
                $result[$id]['companyId'] = $employee->getCompanyId();
            }
        }

        return $this->json([
            'message' => $result
        ]);
    }
    /**
     * @Route("/employees/{id}", name="get_employee", methods={"GET"} )
     */
    public function show(int $id): Response
    {
        
        $entityManager = $this->getDoctrine()->getManager();
        $repo = $entityManager->getRepository(Employee::class);

        $user = $this->getUser();
        $isAdmin = $this->isGranted('ROLE_ADMIN');
        $filters = ["id" => $id];
        
        if(!$isAdmin){
            $filters["companyId"] = $user->getCompanyId();
        }
        $employee = $repo->findOneBy($filters);  
        if(!$employee){
            return $this->json(['message' => "not found"], 404);
        }
        $result = [
            "firstName" => $employee->getFirstName(),
            "lastName" => $employee->getLastName(),
            "email" => $employee->getEmail()
        ];
        if($isAdmin){
            $result["companyId"] = $employee->getCompanyId();
        }
        return $this->json(["message" => $result]);
    }

    /**
     * @Route("/employees", name="add_employee", methods={"POST"})
     */
    public function add(Request $request, ValidatorInterface $validator): Response
    {
        $data = json_decode($request->getContent(), true);

        $constraint = new Assert\Collection([
            
            'firstName' => [new Assert\Type(['type' => 'string']), new Assert\Length(['min' => 3, 'max' => 255])],
            'lastName' => [new Assert\Type(['type' => 'string']), new Assert\Length(['min' => 3, 'max' => 255])],
            'email' => new Assert\Email(),
            'roles' => new Assert\All([new Assert\Type(['int'])]),
            'companyId' => new Assert\Optional(new Assert\Type(['type' => 'int'])),
        ]);
 
        $errors = $validator->validate($data, $constraint);
        if(!empty($errors[0])){
            return $this->json([
                'message' => $errors[0]->getMessage()
            ]);
        }
        


        $entityManager = $this->getDoctrine()->getManager();
        /**
         * @var RoleRepository $roleRepo
         */
        $roleRepo = $entityManager->getRepository(Role::class);
        $user = $this->getUser();
    
        if($this->isGranted('ROLE_ADMIN')){
            if(empty($data["companyId"])){
                return $this->json([
                    'message' => 'The company ID is required.'
                ]);
            }
            $companyId = $data["companyId"];
         
        }else{
            $companyId = $user->getCompanyId();
        }

        $resultRoles = $roleRepo->findRolesByIds($companyId, $data['roles']);

        // dd($resultRoles);
        if(empty($resultRoles)){
            return $this->json([
                'message' => 'Wrong roles'
            ]);
        }
        $employee = new Employee();
        $employee->setEmail($data['email'])
            ->setFirstName($data['firstName'])
            ->setLastName($data['lastName'])
            ->setCompanyId($companyId);
        $entityManager->persist($employee);

        try{
            $entityManager->flush();
        }catch(\Exception $e){
            // hot fix because of validator fail
            return $this->json([
                'message' => 'Something went wrong.'
            ]);
        }
        

        /**
         * @var EmployeeRoleReposiotory $employeeRoleRepo
         */
        $employeeRoleRepo = $entityManager->getRepository(EmployeeRole::class);

        $employeeRoleRepo->insertEmployeeRoles($employee->getId(), array_column($resultRoles, 'id'));
        $email = (new Email())
            ->from('a.burac.unif@gmail.com')
            ->to($employee->getEmail())
            ->text('Lorem ipsum...')
            ->subject('Thanks for signing up!');
        $this->dispatchMessage($email);
        return $this->json([
            'message' => 'Successful'
        ]);
    }

    /**
     * @Route("/employees/{id}", name="update_employee", methods={"PUT"})
     */
    public function update(Request $request, ValidatorInterface $validator, int $id): Response
    {
        $data = json_decode($request->getContent(), true);

        $constraint = new Assert\Collection([
            'firstName' => new Assert\Optional(
                [new Assert\Type(['type' => 'string']), new Assert\Length(['min' => 3, 'max' => 255])]
                ),
            'lastName' => new Assert\Optional(
                [new Assert\Type(['type' => 'string']), new Assert\Length(['min' => 3, 'max' => 255])]
                ),
            'roles' => new Assert\Optional(new Assert\All([new Assert\Type(['int'])]))
        ]);
 
        $errors = $validator->validate($data, $constraint);
        if(!empty($errors[0])){
            return $this->json([
                'message' => $errors[0]->getMessage()
            ]);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $repo = $entityManager->getRepository(Employee::class);

        $filters = ['id' => $id];
        if(!$this->isGranted('ROLE_ADMIN')){
            $filters['companyId'] = $this->getUser()->getCompanyId();;
        }
        $employee = $repo->findOneBy($filters);
        if(!$employee){
            return $this->json(['message' => "not found"], 404);
        }

    
        $employee->setFirstName($data['firstName'] ?? $employee->getFirstName());
        $employee->setLastName($data['lastName'] ?? $employee->getLastName());
        $entityManager->persist($employee);
        $entityManager->flush();


        return $this->json([
            'message' => 'Successful'
        ]);
    }

    /**
     * @Route("/employees/{id}", name="delete_employee", methods={"DELETE"})
     */
    public function delete(int $id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repo = $entityManager->getRepository(Employee::class);

        $filters = ['id' => $id];
        if(!$this->isGranted('ROLE_ADMIN')){
            $filters['companyId'] = $this->getUser()->getCompanyId();;
        }
        $employee = $repo->findOneBy($filters);
        if(!$employee){
            return $this->json(['message' => "not found"], 404);
        }
        if($employee){
            $entityManager->remove($employee);
            $entityManager->flush();
        }
        
        return $this->json([
            'message' => 'Successful'
        ]);
    }
}

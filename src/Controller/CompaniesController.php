<?php

namespace App\Controller;

use App\Entity\Company;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Validator\Constraints as Assert;

class CompaniesController extends AbstractController
{
    /**
     * @Route("/companies", name="get_companies", methods={"GET"} )
     */
    public function index(Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $page = intval($request->query->get('page') ?? 1);
        $page = ($page <= 0) ? 1 : $page; 

        $entityManager = $this->getDoctrine()->getManager();
        $repo = $entityManager->getRepository(Company::class);

        $offset =  $_ENV['PAGE_LIMIT'] * ($page-1);
        $data = $repo->findBy([], null, $_ENV['PAGE_LIMIT'], $offset);
        $result = [];
        foreach($data as $company){
            $result[] = [
                "id" => $company->getId(),
                "name" => $company->getName()];
        }

        return $this->json([
            'message' => $result
        ]);
    }
    /**
     * @Route("/companies/{id}", name="get_company", methods={"GET"} )
     */
    public function show($id): Response
    {   
        $entityManager = $this->getDoctrine()->getManager();
        if(!$this->isGranted('ROLE_ADMIN') && $id != $this->getUser()->getCompanyId()){
            return $this->json(['message' => "Not allowed!"], 403);
        }
        $company = $entityManager->find(Company::class, $id);
        if(!$company){
            return $this->json(['message' => "not found"], 404);
        }
        return $this->json([
            'message' => [
                "name" => $company->getName()]
        ]);
    }

    /**
     * @Route("/companies", name="add_company", methods={"POST"})
     */
    public function add(Request $request, ValidatorInterface $validator): Response
    {

        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $data = json_decode($request->getContent(), true);
        $entityManager = $this->getDoctrine()->getManager();
        $company = new Company();
        $company->setName($data);
        $entityManager->persist($company);
        $entityManager->flush();

        return $this->json([
            'message' => 'Successful',
        ]);
    }

    /**
     * @Route("/companies/{id}", name="update_company", methods={"PUT"})
     */
    public function update(Request $request, ValidatorInterface $validator, int $id): Response
    {
        
        $data = json_decode($request->getContent(), true);
        $constraint = new Assert\Collection([
            'name' => new Assert\Optional(
                    [new Assert\Type(['type' => 'string']), new Assert\Length(['min' => 3, 'max' => 255])]
                )
        ]);
        $errors = $validator->validate($data, $constraint);
        if(!empty($errors[0])){
            return $this->json([
                'message' => $errors[0]->getMessage()
            ]);
        }
        $entityManager = $this->getDoctrine()->getManager();

        if(!$this->isGranted('ROLE_ADMIN') && $id != $this->getUser()->getCompanyId()){
            return $this->json(['message' => "Not allowed!"], 403);
        }
        $company = $entityManager->find(Company::class, $id);
        if(!$company){
            return $this->json(['message' => "not found"], 404);
        }

        $company->setName($data['name']);
        $entityManager->persist($company);
        $entityManager->flush();

        return $this->json([
            'message' => 'Successful'
        ]);
    }

    /**
     * @Route("/companies/{id}", name="delete_company", methods={"DELETE"})
     */
    public function delete(int $id): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $entityManager = $this->getDoctrine()->getManager();
        $company = $entityManager->find(Company::class, $id);
        if(!$company){
            return $this->json(['message' => "not found"], 404);
        }
        if($company){
            $entityManager->remove($company);
            $entityManager->flush();
        }

        return $this->json([
            'message' => 'Successful'
        ]);
    }
}

<?php
namespace App\MessageHandler;

use Exception;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;


class EmailNotificationHandler implements MessageHandlerInterface
{
    /**
     * Mailer
     *
     * @var MailerInterface
     */
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;    
    }
    public function __invoke(Email $email)
    {
        try{
            $this->mailer->send($email);
        }catch(TransportExceptionInterface  $e){
            print_r($e->getDebug());
        }
        
    }
}
